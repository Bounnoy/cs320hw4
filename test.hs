-- Bounnoy Phanthavong, CS 320 - Homework 4
module TestHW4 where
import Test.HUnit
import HW4

-- Evaluate (1 + 2) * 3
exp1 = Mult (Add (Val 1) (Val 2)) (Val 3)
test1 = TestCase (assertEqual "Evaluating (1 + 2) * 3 ..." 9 (ev exp1))

-- Evaluate 4 / (3 - 1) DIVISION OK
exp2 = Div (Val 4) (Sub (Val 3) (Val 1))
test2 = TestCase (assertEqual "Evaluating 4 / (3 - 1) ..." 2 (ev exp2))

-- Evaluate 4 / (1 * 3) DIVISION NOT OK
exp3 = Div (Val 4) (Mult (Val 1) (Val 3))
test3 = TestCase (assertEqual "Evaluating 4 / (1 * 3) ..." (-1) (ev exp3))

-- Display (1 + 2) * 3
exp4 = Mult (Add (Val 1) (Val 2)) (Val 3)
test4 = TestCase (assertEqual "Printing equation ..." "((1 + 2) * 3)" (pr exp4))

-- Display 4 / (3 - 1) DIVISION OK
exp5 = Div (Val 4) (Sub (Val 3) (Val 1))
test5 = TestCase (assertEqual "Printing equation ..." "(4 / (3 - 1))" (pr exp5))

-- Display 4 / (1 * 3) DIVISION NOT OK
exp6 = Div (Val 4) (Mult (Val 1) (Val 3))
test6 = TestCase (assertEqual "Printing equation ..." "(4 / (1 * 3))" (pr exp6))

-- Automate tests.
tests = TestList [
  TestLabel "test1" test1,
  TestLabel "test2" test2,
  TestLabel "test3" test3,
  TestLabel "test4" test4,
  TestLabel "test5" test5,
  TestLabel "test6" test6
  ]
