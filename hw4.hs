-- Bounnoy Phanthavong, CS 320 - Homework 4
module HW4 where

-- Data abstractions.
data Operand
  = Val Integer
  | Add Operand Operand
  | Sub Operand Operand
  | Mult Operand Operand
  | Div Operand Operand
  deriving (Eq, Show)

-- Evaluate an expression.
ev :: Operand -> Integer
ev (Val x) = x
ev (Add x y) = (ev x) + (ev y)
ev (Sub x y) = (ev x) - (ev y)
ev (Mult x y) = (ev x) * (ev y)
ev (Div x y) = if ((ev x) == 0 || (ev y) == 0 || (ev x) `mod` (ev y) /= 0) then (-1) else (ev x) `div` (ev y)

-- Print an expression.
pr :: Operand -> String
pr (Val x) = show x
pr (Add x y) = "(" ++ (pr x) ++ " + " ++ (pr y) ++ ")"
pr (Sub x y) = "(" ++ (pr x) ++ " - " ++ (pr y) ++ ")"
pr (Mult x y) = "(" ++ (pr x) ++ " * " ++ (pr y) ++ ")"
pr (Div x y) = "(" ++ (pr x) ++ " / " ++ (pr y) ++ ")"

-- Build expressions.
build :: [Integer] -> [Operand]
build [] = []
build [x] = [Val x]
build (x:y:xs)
  | length(x:y:xs) == 2 =
    [(Add (Val x) (Val y))] ++
    [(Sub (Val x) (Val y))] ++
    [(Mult (Val x) (Val y))] ++
    [(Div (Val x) (Val y))]
  | length(x:y:xs) == 3 =
    [Add a (Val b) | a <- (build ([x,y])), b <- xs] ++
    [Sub a (Val b) | a <- (build ([x,y])), b <- xs] ++
    [Mult a (Val b) | a <- (build ([x,y])), b <- xs] ++
    [Div a (Val b) | a <- (build ([x,y])), b <- xs] ++
    [Add (Val x) b | b <- (build ([x,y]))] ++
    [Sub (Val x) b | b <- (build ([x,y]))] ++
    [Mult (Val x) b | b <- (build ([x,y]))] ++
    [Div (Val x) b | b <- (build ([x,y]))]
  | length(x:y:xs) == 4 =
    [Add a (Val b) | a <- (build ([x,y,head xs])), b <- tail xs] ++
    [Sub a (Val b) | a <- (build ([x,y,head xs])), b <- tail xs] ++
    [Mult a (Val b) | a <- (build ([x,y,head xs])), b <- tail xs] ++
    [Div a (Val b) | a <- (build ([x,y,head xs])), b <- tail xs] ++
    [Add (Val x) b | b <- (build (y:xs))] ++
    [Sub (Val x) b | b <- (build (y:xs))] ++
    [Mult (Val x) b | b <- (build (y:xs))] ++
    [Div (Val x) b | b <- (build (y:xs))] ++
    [Add a b | a <- (build ([x,y])), b <- (build xs)] ++
    [Sub a b | a <- (build ([x,y])), b <- (build xs)] ++
    [Mult a b | a <- (build ([x,y])), b <- (build xs)] ++
    [Div a b | a <- (build ([x,y])), b <- (build xs)]
  | length(x:y:xs) > 4 = error "array must be less than 4"

-- Create all valid expressions.
numList = replicate 4 10
generate = build numList
expressions = [(pr x) ++ " = " ++ show y | x <- generate, let y = ev x, y >= 0, y < 10]

-- Print one expression per line.
printAll [] = return ()
printAll (x:y) = do putStrLn (show x); printAll y

main :: IO ()
main = printAll expressions
